#include "Vector.h"
#include <iostream>

Vector::Vector(int n)
{
	if (n < MIN_CAPACITY) 
	{
		n = 2; //has to be atleast 2
	}
	_elements = new int[n]; //allocating the first memory for the array
	
	_capacity = n;
	_resizeFactor = n;
	_size = 0;
}

Vector::~Vector()
{
	delete[] _elements; //deletes the array 
	_elements = nullptr;
}

int Vector::size() const
{
	return _size;
}


int Vector::capacity() const
{
	return _capacity;
}

int Vector::resizeFactor() const
{
	return _resizeFactor;
}

bool Vector::empty() const
{
	return _size == 0; //if the size = 0, there is no elements in the vector
}

void Vector::push_back(const int& val)
{
	if (_capacity == _size) // if capacity is full
	{
		int* tmpArr = new int[_capacity + _resizeFactor]{NULL}; //expanding
		std::copy(_elements, _elements + _size , tmpArr); //copying from _elements to empArr
		delete[] _elements; //deleting the previous array
		_elements = tmpArr; //setting the new Vector's array
		
		_capacity = _capacity + _resizeFactor; 
		_elements[_size] = val; //adding the element

	}

	_elements[_size] = val; //adding the element
	_size++; //updating size - 1 element added 
}


int Vector::pop_back()
{
	int result = 0;

	if (empty()) //if there is no elements
	{
		std::cout << "error: pop from empty vector";
		result = POP_EMPTY_CODE;
	}
	else
	{
		result = _elements[_size - 1]; //saving element
		_elements[_size - 1] = NULL; //removing element
		_size--; //updating size
	}

	return result;
}

void Vector::reserve(int n)
{
	if (_capacity < n) 
	{ //need to expand
		int newCapacity = 0; 
		if (n % this->_resizeFactor == 0) 
		{
			newCapacity = n; //the new capacity here need to be just n
		}					// because there is not problem "filling" the new capacity with resizeFactors
		else
		{				
			newCapacity = (n / this->_resizeFactor + 1)*this->_resizeFactor; //but here we have to get the whole devision of n with reiszeFactor, and then add a 1 (and this will be the number of resize factors)
																		     //because we want to "complete" to n 
		}

		int* tmpArr = new int[newCapacity] {NULL}; //creating new array
		std::copy(_elements, _elements + _size, tmpArr); //copying from _elements to empArr
		delete[] _elements; //deleting the previous array
		_elements = tmpArr; //setting the new Vector's array
		
		_capacity = newCapacity; //setting new capacity

	}
}

void Vector::resize(int n)
{
	if (n > _capacity)
	{
		reserve(n);
	}
	//if n < capacity there is no need to delete the elements, because they are unreachable
	this->_size = n;
}

void Vector::assign(int val)
{
	int i = 0;

	for (i = 0; i < _size; i++)
	{
		_elements[i] = val; //assign val to the element
	}
}


void Vector::resize(int n, const int& val)
{
	int i = 0;

	if (n > _capacity)
	{
		reserve(n);
	}
	//if n < capacity there is no need to delete the elements, because they are unreachable
	_size = n;

	
	//assigning elements to val
	for (i = 0; i < _size; i++)
	{
		_elements[i] = val; //assign val to the element
	}
}


int& Vector::operator[](int n) const
{
	if (n > _size - 1 || n < 0) //invalid index
	{
		std::cout << "invalid index at Vector::operator[]";
		_exit(1);
	}
	
	return _elements[n];
}

/*
	copy constructor - copies fields from another Vector
	input: the other Vector
	output: none
*/
Vector::Vector(const Vector& other)
{
	this->_capacity = other.capacity();
	this->_size = other.size();
	this->_resizeFactor = other.resizeFactor();

	this->_elements = new int[other.capacity()];

	std::copy(other._elements, other._elements + other._size, _elements); //copying elements
}


Vector& Vector::operator=(const Vector& other)
{
	this->_capacity = other.capacity();
	this->_size = other.size();
	this->_resizeFactor = other.resizeFactor();

	delete[] this->_elements; //freeing the current array
	this->_elements = nullptr;

	this->_elements = new int[other.capacity()];

	std::copy(other._elements, other._elements + other._size, this->_elements); //copying the elements

	return *this;
}


